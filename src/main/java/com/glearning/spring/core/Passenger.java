package com.glearning.spring.core;

public class Passenger {

	// coupling
	private UberGo driver;
	
	private UberPrime primeDriver;

	public Passenger(UberGo driver, UberPrime uberPrime) {
		this.driver = driver;
		this.primeDriver = uberPrime;
	}

	public void commute(String source, String destination, long distance) {
		if (distance < 15) {
			driver.trip(source, destination);
		} else {
			primeDriver.primeRide(source, destination);
		}
	}

}
