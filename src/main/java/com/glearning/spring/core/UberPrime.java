package com.glearning.spring.core;

public interface UberPrime {
	
	void primeRide(String source, String destination);

}
